package com.bradkarels.kryo;

import com.esotericsoftware.kryo.Kryo;

public interface KryoSerializer<T> {
	
	public byte[] write(Kryo kryo, T t);
	
	public T read(Kryo kryo, byte[] bytes, Class<T> cla55);

}