package com.bradkarels.kryo;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

public class Serializer<T> implements KryoSerializer<T> {

	@Override
	public byte[] write(Kryo kryo, T t) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		Output output = new Output(baos);
		kryo.writeObject(output, t);
		output.close();
		return baos.toByteArray();
	}

	@Override
	public T read(Kryo kryo, byte[] bytes, Class<T> cla55) {
		ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
		Input input = new Input(bais);
//		T retval = kryo.readObject(input, cla55);
//		return retval;
		return kryo.readObject(input, cla55);
	}
}