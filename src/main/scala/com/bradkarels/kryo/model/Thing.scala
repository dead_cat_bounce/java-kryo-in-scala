package com.bradkarels.kryo.model

class Thing {
  var name: String = ""
  
  def this(name: String) {
    this()
    this.name = name
  }
}

object Thing {
  def apply(name: String) = new Thing(name)
}
