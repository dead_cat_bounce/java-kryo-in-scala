package com.bradkarels.kryo.app

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext

import com.esotericsoftware.kryo.Kryo

import com.bradkarels.kryo.model.Thing
import com.bradkarels.kryo._

object SimpleSerializer {
  def main(args: Array[String]) {
    val conf = new SparkConf().setAppName("Simple Kryo App")
    conf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")

    val sc = new SparkContext(conf)
    
    val thing: Thing = Thing("Pete Jones")
    
    println(s"My name is: ${thing.name}")
    
    val serializer: KryoSerializer[Thing] = new Serializer()
    
    //TODO: Use Pooled Kryo objects
    val kryo: Kryo = new Kryo()
    
    val serializedBytes: Array[Byte] = serializer.write(kryo, thing)
    
    println(s"serializedBytes.length = ${serializedBytes.length}")
    
    val deserializedThing: Thing = serializer.read(kryo, serializedBytes, classOf[Thing])
    
    println(s"What's your name deserializedThing?")
    
    println(s"My name is ${deserializedThing.name}")
  }
}