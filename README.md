## java-kryo-in-scala
Since scala Kryo documentation is sparse, this is an example of how to roll Kryo into a scala app using Java.

### Build the Application
The project is an SBT application with the assembly plugin wired up.  From the project root:

> `$ sbt`

> `> ;clean;compile;assembly`

The end result should be `java-kryo-in-scala.jar` under `java-kryo-in-scala/target/scala-2.10/`.

### Deploy to Spark local (or other cluster)
Assuming you are running a local Spark cluster you can execute things as follows (from SPARK_HOME):

> '$ bin/spark-submit --class com.bradkarels.kryo.app.SimpleSerializer --master spark://127.0.0.1:7077 /path/to/your/java-kryo-in-scala/target/scala-2.10/java-kryo-in-scala.jar`

### Output
If all went according to plan you should see the following in your terminal session:

> My name is: Pete Jones

> serializedBytes.length = 12

> What's your name deserializedThing?

> My name is Pete Jones
